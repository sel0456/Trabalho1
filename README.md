# Trabalho1
## Descrição
O trabalho consiste na codificação de três funções em diferentes arquivos e uma função main que chame essas funções e as aplique em um array. Os códigos são desenvolvidos na linguagem de programação C.

## Utilização
Para utilizar o programa, basta colocar os valores do array na linha de comando. Para um array [3,2,5,1] tem-se:

*>> ./main 3 2 5 1*

Para ajuda, basta executar:

*>> ./main --help*


